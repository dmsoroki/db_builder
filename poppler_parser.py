import subprocess
import re
import platform
from paragraph import Paragraph


class PDFReader(object):
    def __init__(self):
        self.file_name = None
        self.paragraph_separators = ('.\n ', '!\n ', '?\n ', '\n\n')
        self.page_separator = '\f'
        system = platform.system()
        if system == 'Windows':
            self.pdfinfo_cmd = 'bin/pdfinfo.exe'
            self.pdftotext_cmd = 'bin/pdftotext.exe'
        elif system == 'Darwin':
            self.pdfinfo_cmd = 'pdfinfo'
            self.pdftotext_cmd = 'pdftotext'
        else:
            assert False, 'system {} is not tested'.format(system)

    def open(self, fname):
        self.file_name = str(fname)

    def close(self):
        pass

    def _find_page_starts(self, string):
        return [i for i, letter in enumerate(string) if letter == self.page_separator]

    def _cleanup(self, string):
        string = re.sub(r'([_()/#"{}\[\]+=@$%^&*|<>])', ' ', string)
        string = re.sub(r"([~'])", '', string)#string = re.sub(r"([a-zA-Z0-9~'])", '', string)### ---- modify
        string = string.replace('-', '')
        return string

    def read_paragraphs(self):
        text = self.convert()
        page_starts = self._find_page_starts(text)
        text = text.replace(self.page_separator, '')
        regexp_pattern = '|'.join(map(re.escape, self.paragraph_separators))
        splitted_text = re.split(regexp_pattern, text)
        page_index = 0
        length = 0
        for splitted in splitted_text:
            length += len(splitted)
            while length > page_starts[page_index]:
                page_index += 1
            splitted = self._cleanup(splitted)
            paragraph = Paragraph(splitted, page_index)
            yield paragraph
        return

    def pages(self):
        result = subprocess.check_output([self.pdfinfo_cmd, '-enc', 'UTF-8', self.file_name])
        result = result.decode('utf-8', errors='ignore').split('\n')
        for line in result:
            if 'Pages:' in line:
                pages = int(line.split()[1])
                return pages
        raise FileNotFoundError

    def read_page(self, pagenum):
        result = subprocess.check_output(
            [self.pdftotext_cmd,
             '-enc', 'UTF-8',
             '-layout',
             self.file_name, '-',
             '-f', str(pagenum),
             '-l', str(pagenum),
             '-eol', 'unix'])
        return result.decode('utf-8', errors='ignore')

    def split_to_paragraphs(self, text):
        text = self._cleanup(text)
        regexp_pattern = '|'.join(map(re.escape, self.paragraph_separators))
        splitted_text = re.split(regexp_pattern, text)
        return list(filter(lambda string: len(string) > 0, splitted_text))

    def convert(self):
        result = subprocess.check_output(
            [self.pdftotext_cmd,
             '-enc', 'UTF-8',
             '-layout',
             str(self.file_name), '-',
             '-eol', 'unix'])
        return result.decode('utf-8', errors='ignore')


