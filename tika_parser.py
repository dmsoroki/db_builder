import tika
import re
from tika import parser
from bs4 import BeautifulSoup
from paragraph import Paragraph


class PDFReader(object):
    def __init__(self):
        self._pages = None

    def open(self, fname):
        try:
            parsed = parser.from_file(fname, xmlContent=True)
            content = parsed['content']
        except:
            print('can not parse', fname)
            content = None
        
        if content is None:
            print('can not read', fname)
            self._pages = []
        else:
            self._pages = content.split('<div class="page"><p />')

    def close(self):
        pass

    def _cleanup(self, string):
        string = re.sub(r'([_()/#"{}\[\]+=@$%^&*|<>])', ' ', string)
        string = re.sub(r"([~'])", '', string)#string = re.sub(r"([a-zA-Z0-9~'])", '', string)### ---- modify
        string = string.replace('-', '')
        return string

    def read_paragraphs(self):
        for page_number, page in enumerate(self._pages):
            soup = BeautifulSoup(page, 'html.parser')
            for parag_soup in soup.find_all('p'):
                text = parag_soup.get_text()
                text = self._cleanup(text)
                paragraph = Paragraph(text, page_number - 1)
                yield paragraph

    def pages(self):
        return len(self._pages)



