import numpy as np
import pymorphy2
from nltk.stem import WordNetLemmatizer### --- add
from nltk import pos_tag### --- add
from nltk.corpus import wordnet
import re
import json
import tika_parser
import os
import sqlite3
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from tqdm import trange
from multiprocessing.pool import ThreadPool
from collections import defaultdict
from pathlib import Path
import cx_Oracle


_MORPH = pymorphy2.MorphAnalyzer()
_LEM = WordNetLemmatizer()# english lemmatizator ### --- add
_POOL = ThreadPool(processes=8)
wordnet.ensure_loaded()


with open('data/collocations.txt', encoding='utf-8') as f:
    words = f.read().split('\n')
    _COLLOCATIONS = set(words)



def db_connect():
    dsn_tns = cx_Oracle.makedsn('10.254.56.212', '1521', service_name='ORCL')
    conn = cx_Oracle.connect(user='adm', password='formula306', dsn=dsn_tns)
    return conn


def get_folders():
    return ['D:/SMP_LIB/Файловый архив']
    #dsn_tns = cx_Oracle.makedsn('10.254.56.212', '1521', service_name='ORCL')
    #conn = cx_Oracle.connect(user='adm', password='formula306', dsn=dsn_tns)
    #cursor = conn.cursor()
    #
    #result = []
    #for (name,) in cursor.execute('SELECT name FROM RECOGN_FOLDER'):
    #    result.append(name)
    #
    #conn.close()
    #
    #return result


def _is_latin(word):
    for c in word:
        if ord(c) < ord('a') or ord(c) > ord('z'):
            return False
    return True


def _read_pdf(file_name):
    reader = tika_parser.PDFReader()
    reader.open(file_name)
    prg = []; page = []
    for paragraph in reader.read_paragraphs():
        prg.append(paragraph.text)
        page.append(paragraph.page_number)
    print('#paragraphs', len(prg))
    return prg, page


def _create_db(fname, words):
    if os.path.isfile(fname):
        os.remove(fname)
    conn = sqlite3.connect(fname)
    cursor = conn.cursor()
    cursor.execute('CREATE TABLE Files(id_file INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NULL );')
    cursor.execute('CREATE TABLE Vocab(id_word INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, word TEXT NULL );')
    for word in words:
        que = 'INSERT INTO Vocab (word) values ("{}")'.format(word)
        cursor.execute(que)
    conn.commit()
    cursor.execute('CREATE TABLE Texts(id_parag INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_file INTEGER, parag TEXT NULL, vector TEXT NULL, docLen INTEGER, page_number INTEGER, doc2vec TEXT NULL);')
    cursor.execute('CREATE TABLE InvIndex(id_event INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_word INTEGER, id_parag INTEGER, cnt INTEGER);')
    return conn, cursor


def stringReplace(s):
    s = s.replace('\n',' ')
    s = s.replace(';','')
    s = s.replace(',','')
    s = s.replace('.','')
    s = s.replace('!','')
    s = s.replace('?','')
    s = s.replace('-','')
    s = s.replace(':','')
    s = s.replace('"','').replace(u'\xbb','').replace(u'\xab','')
    s = s.replace('(','').replace(')','')
    s = re.sub(r'([^а-яёА-ЯЁa-zA-Z ])','',s)### ---- modify
    #s = s.replace('/','')
    return s


def normalize_paragraph(text):
    paragraph = stringReplace(text.lower())
    words = paragraph.split(' ')
    normalized_words = []
    for word in words:
        if len(word) == 0:
            continue

        if not _is_latin(word):
            # normalization (russian)
            norm_word = _MORPH.parse(word)
            word = norm_word[0].normal_form
        else:
            # lemmarization (english)
            tag = pos_tag([word])[0][1][0].lower()### --- add
            if tag not in ['a', 's', 'r', 'n', 'v']: tag = 'n'### --- add
            word = _LEM.lemmatize(word, tag)### --- add

        normalized_words.append(word)

    # merge verb with ne
    normalized_words = merge_ne(normalized_words)

    # word collocations
    normalized_words = merge_collocations(normalized_words, _COLLOCATIONS)
    return normalized_words


def normalize_text(paragraphs):
    normalized = []

    async_results = []
    for parag in paragraphs:
        future = _POOL.apply_async(normalize_paragraph, [parag])
        async_results.append(future)
    for future in async_results:
        normalized.append(future.get())


    #for paragraph in paragraphs:
    #    normalized.append(normalize_paragraph(paragraph))

    return normalized


def docVecCalc(model, normText):
    docsV = []
    for text in normText:
        v = np.zeros(model.vector_size)
        K = 0
        for wd in text:
            if wd in model.wv.vocab:# and wd not in stopW: # --- !!!!!!!!!!!!!!!!!! перенести фильрацию стоп слов в обучение модели !!!!!!!!!!!!!!!!!!
                v += model.wv[wd]
                K += 1
        if K > 0: v = v / K
        docsV.append(v)
    return docsV


def merge_ne(words):
    k = []
    for i in range(len(words) - 1):
        if words[i] == 'не':
            if len(words[i + 1]) < 3: continue
            w = _MORPH.parse(words[i + 1])[0]
            if w.tag.case == None:
                words[i] = ' '.join([words[i], words[i + 1]])
                k.append(i + 1)

    return [word for i, word in enumerate(words) if i not in k]


def merge_collocations(words, collocat):
    merged_paragraph = []
    last_word_merged = False
    for i in range(len(words)-1):
        first, second = words[i], words[i+1]
        merged_word = ' '.join([first, second])
        if merged_word in collocat:
            merged_paragraph.append(merged_word)
            last_word_merged = True
        else:
            if not last_word_merged:
                merged_paragraph.append(first)
            last_word_merged = False
    if len(words) > 0 and not last_word_merged:
        merged_paragraph.append(words[-1])

    return merged_paragraph


def _write_inv_ind(cursor, words, word_ids):
    prg_id = cursor.lastrowid

    inv_ind = defaultdict(int)
    for word in words:
        inv_ind[word] += 1

    for word in inv_ind:
        if word not in word_ids:
            que = 'INSERT INTO Vocab (word) VALUES ("{}")'.format(word)
            cursor.execute(que)
            word_ids[word] = cursor.lastrowid

        w_id = word_ids[word]
        cnt = inv_ind[word]
        que = 'INSERT INTO InvIndex (id_word, id_parag, cnt) VALUES ({}, {}, {})'.format(w_id, prg_id, cnt)
        cursor.execute(que)


def CalcInvIndDB(fnames, fname, model):
    #conn, cursor = _create_db(fname, model.wv.vocab.keys())
    conn = db_connect()
    cursor = conn.cursor()

    word_ids = {}
    for (id_word, word) in cursor.execute('SELECT id_word, word FROM Vocab'):
        word_ids[word] = id_word

    existed_files = set()
    for (fname,) in cursor.execute('SELECT name FROM Files'):
        #base_name = os.path.basename(fname)
        existed_files.add(Path(fname))

    for iname in trange(len(fnames)):
        nm = fnames[iname]
        path = Path(nm)
        
        if path in existed_files:
            continue

        que = 'INSERT INTO Files (name) VALUES ("{}")'.format(nm)
        cursor.execute(que)
        f_id = cursor.lastrowid
        
        prg, page = _read_pdf(nm)
        nmText = normalize_text(prg)
        docV = docVecCalc(model, nmText)
        print(len(docV))
        for j in range(len(nmText)):
            vec = ' '.join(str(comp) for comp in docV[j])
            que = 'INSERT INTO Texts (id_file, parag, vector, docLen, page_number) VALUES ({}, "{}", "{}", {}, {})'.format(f_id, prg[j], vec, len(nmText[j]), page[j])
            cursor.execute(que)
            _write_inv_ind(cursor, nmText[j], word_ids)
        conn.commit()
    conn.close()
