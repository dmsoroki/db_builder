import os
from gensim.models import Word2Vec
from utils import CalcInvIndDB, get_folders
from pathlib import Path
from tqdm import trange


folders = get_folders()


def find_docs(paths, ext=('.pdf', '.docx', '.doc', '.pptx', '.ppt', '.xls', '.xlsx', '.djvu')):
    result = []
    for path in paths:
        for root, subdirs, files in os.walk(path):
            for file in files:
                if file.lower().endswith(ext):
                    result.append(os.path.join(root, file))

    return result


pdf_files = find_docs(folders)


# file size < 1Gb
pdf_files = list(
    filter(
        lambda x: os.path.getsize(x) < 1024 ** 3, 
        pdf_files
    )
)

model = Word2Vec.load('data/word2vec')
CalcInvIndDB(pdf_files, 'trd_bd.sqlite', model)
