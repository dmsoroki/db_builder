class Paragraph(object):
    def __init__(self, text, page_index):
        self.text = text
        self.page_index = page_index
        self.page_number = page_index + 1

    def __repr__(self):
        return 'page: {}, text: {}'.format(self.page_number, self.text)
